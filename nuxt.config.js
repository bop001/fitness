export default {
  mode: 'universal',

  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],

    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  env: {
    apiUrl: 'http://fitness-backend.dev-team.su/'
  },

  loading: { color: 'red', height: '5px' },

  css: [
    'element-ui/lib/theme-chalk/index.css',
    '@/theme/index.sass',
    '@/assets/fonts/index.sass'
  ],

  plugins: [
    '@/plugins/globals',
    '@/plugins/axios',
    { src: '@/plugins/maskedInput.js', mode: 'client' },
    '@/plugins/vueSocialSharing'
  ],

  buildModules: [
    '@nuxtjs/eslint-module',
    ['@nuxtjs/moment', { locales: ['ru'] }]
  ],

  modules: ['@nuxtjs/axios'],

  axios: {
    baseURL: 'http://fitness-backend.dev-team.su/'
  },

  build: {
    transpile: [/^element-ui/],
    extend(config, ctx) {}
  }
}
