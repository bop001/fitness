/* eslint-disable no-use-before-define */

import {
  compose,
  map,
  toPairs,
  fromPairs,
  is,
  cond,
  identity,
  T,
  concat
} from 'ramda'

export const toCamel = str =>
  str.replace(/[-_]([a-z])/g, m => m[1].toUpperCase())
export const toSnake = str =>
  str.replace(/([A-Z])/g, x => concat('_', x.toLowerCase()))
/**
 * Function used to transform a fetched data
 *
 * @param param
 * @return {*}
 */

export const toFetch = cond([
  [is(Array), v => toFetchCollection(v)],
  [is(Object), v => toFetchObject(v)],
  [T, identity]
])

/**
 * Function used to transform a fetched collection
 *
 * @param param
 * @return [Array]
 */
const toFetchCollection = map(toFetch)

/**
 * Function used to transform a fetched object
 *
 * @param param
 * @return {{}}
 */
const toFetchObject = compose(
  fromPairs,
  map(([key, value]) => [toCamel(key), toFetch(value)]),
  toPairs
)

/**
 * Function used to transform a toSend data
 *
 * @param param
 * @return {*}
 */
export const toSend = cond([
  [is(Array), v => toSendCollection(v)],
  [is(Object), v => toSendObject(v)],
  [T, identity]
])

/**
 * Function used to transform a collection to be toSend
 *
 * @param param
 * @return [Array]
 */
const toSendCollection = map(toSend)

/**
 * Function used to transform a object to be toSend
 *
 * @param param
 * @returns {{}}
 */
const toSendObject = compose(
  fromPairs,
  map(([key, value]) => [toSnake(key), toSend(value)]),
  toPairs
)
