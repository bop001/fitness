export default function({ store, redirect }) {
  if (!store.getters['index/error']) {
    redirect('/login?message=login')
  }
}
