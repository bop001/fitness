export const state = () => ({
  error: null,
  success: null
})

export const actions = {
  nuxtServerInit({ dispatch }) {
    dispatch('auth/autoLogin')
  }
}

export const mutations = {
  setError(state, error) {
    state.error = error
  },
  setSuccess(state, success) {
    state.success = success
  },
  clearError(state) {
    state.error = null
  }
}

export const getters = {
  error: state => state.error,
  success: state => state.success
}
