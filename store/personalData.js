export const state = () => ({
  data: {}
})

export const actions = {
  async fetchPersonalData({ commit }) {
    try {
      const payload = await this.$axios.$get(`/api/personal_data`)
      commit('setData', payload)
    } catch (e) {
      commit('setError', e, { root: true })
    }
  }
}

export const getters = {
  getData: state => state.data
}

export const mutations = {
  setData(state, payload) {
    const { data } = payload
    state.data = data
  }
}
