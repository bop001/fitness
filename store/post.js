import { toSend } from '@/helpers/transform'

export const state = () => ({
  items: [],
  links: {},
  meta: {},
  dataPost: {},
  sameData: {},
  comments: [],
  rating: []
})
export const actions = {
  async fetchPosts({ commit }, params) {
    let query = ''
    for (const param in params) {
      if (params[param] !== '') {
        query += `${param}=${params[param]}&`
      }
    }
    await this.$axios
      .$get(`/api/articles?${query}`)
      .then(response => {
        commit('setPosts', response)
      })
      .catch(e => {
        commit('setError', e, { root: true })
        throw e
      })
  },
  async fetchPostsById({ commit }, params) {
    await this.$axios
      .$get(`/api/articles/${params}`)
      .then(response => {
        commit('setPostsById', response)
      })
      .catch(e => {
        commit('setError', e, { root: true })
        throw e
      })
  },
  async fetchComments({ commit }, params) {
    await this.$axios
      .$get(`/api/articles/${params}/comments`)
      .then(response => {
        commit('setComments', response.data)
      })
      .catch(e => {
        commit('setError', e, { root: true })
        throw e
      })
  },
  async fetchSamePosts({ commit }, params) {
    await this.$axios
      .$get(`/api/articles/${params}/same`)
      .then(response => {
        commit('setSamePosts', response)
      })
      .catch(e => {
        commit('setError', e, { root: true })
        throw e
      })
  },
  async addComment({ commit }, params) {
    await this.$axios
      .$post(`api/articles/add_comment`, toSend(params))
      .then(response => {
        commit('setComments', response.data)
      })
      .catch(e => {
        commit('setError', e, { root: true })
      })
  },
  async addLike({ commit }, params) {
    await this.$axios
      .$get(`/api/articles/like/${params}`)
      .then(response => {
        if (response.message) {
          commit('setError', response.message, { root: true })
        } else {
          return response
        }
      })
      .catch(e => {
        commit('setError', e, { root: true })
      })
  },
  async changeRating({ commit }, params) {
    await this.$axios
      .$post(`/api/articles/comment/rating`, toSend(params))
      .then(response => {
        if (response.message) {
          commit('setError', response.message, { root: true })
        } else {
          commit('changeRating', response)
        }
      })
      .catch(e => {
        commit('setError', e, { root: true })
      })
  }
}

export const getters = {
  getPosts: state => state.items,
  getLinks: state => state.links,
  getMeta: state => state.meta,
  getPostsById: state => state.dataPost,
  getComments: state => state.comments,
  getSamePosts: state => state.sameData,
  getRating: state => state.rating
}

export const mutations = {
  setPosts(state, payload) {
    const { data, meta, links } = payload
    state.items = data
    state.links = links
    state.meta = meta
  },
  setPostsById: (state, payload) => (state.dataPost = payload.data),
  setComments: (state, payload) => (state.comments = payload),
  setSamePosts: (state, payload) => (state.sameData = payload.data),
  changeRating: (state, payload) => {
    state.rating = state.comments.map(comment => {
      if (comment.comment_id === payload.comment_id)
        comment.rating = payload.rating
      else {
        comment.answers.map(answer => {
          if (answer.comment_id === payload.comment_id)
            return (answer.rating = payload.rating)
        })
      }
      /* switch (comment.comment_id) {
        case payload.comment_id:
          comment.rating = payload.rating
          break
        default:
          comment.answers.map(answer => {
            switch (answer.comment_id) {
              case payload.comment_id:
                answer.rating = payload.rating
                break
            }
          })
          break
      } */
    })
  }
}
