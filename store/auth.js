import Cookie from 'cookie'
import Cookies from 'js-cookie'
import jwtDecode from 'jwt-decode'
import { toSend, toFetch } from '@/helpers/transform'

export const state = () => ({
  token: null,
  cities: []
})

export const mutations = {
  setToken(state, token) {
    state.token = token
  },
  clearToken(state) {
    state.token = null
  },
  getCities: (state, payload) =>
    (state.cities = payload.map(item => {
      return { value: Number(`${item.cityId}`), label: `${item.title}` }
    }))
}

export const actions = {
  async login({ commit, dispatch }, payload) {
    try {
      const { token } = await this.$axios.$post('api/login', toSend(payload))
      dispatch('setToken', token)
      this.$router.push('/news?page=1')
    } catch (e) {
      commit('setError', e, { root: true })
    }
  },
  async createUser({ commit }, payload) {
    try {
      await this.$axios.$post('api/register', toSend(payload))
    } catch (e) {
      commit('setError', e, { root: true })
      throw e
    }
  },
  async getCities({ commit }) {
    await this.$axios
      .$get(`/api/cities`)
      .then(response => {
        commit('getCities', toFetch(response.data))
      })
      .catch(e => {
        commit('setError', e, { root: true })
      })
  },
  async forgot({ commit }, params) {
    await this.$axios
      .$post(`/api/forgot_password`, toSend(params))
      .then(response => {
        if (response.message) {
          commit('setSuccess', response.message, { root: true })
        } else {
          return response
        }
      })
      .catch(e => {
        commit('setError', e, { root: true })
      })
  },
  setToken({ commit }, token) {
    this.$axios.setToken(token, 'Bearer')
    commit('setToken', token)
    Cookies.set('jwt-token', token)
  },
  logout({ commit }) {
    this.$axios.setToken(false)
    commit('clearToken')
    Cookies.remove('jwt-token')
  },
  autoLogin({ dispatch }) {
    const cookieStr = process.browser
      ? document.cookie
      : this.app.context.req.headers.cookie
    const cookies = Cookie.parse(cookieStr || '') || {}
    const token = cookies['jwt-token']
    if (isJWTValid(token)) {
      dispatch('setToken', token)
    } else {
      dispatch('logout')
    }
  }
}

export const getters = {
  isAuthenticated: state => Boolean(state.token),
  token: state => state.token,
  getCitiesList: state => state.cities
}
function isJWTValid(token) {
  if (!token) {
    return false
  }
  const jwtData = jwtDecode(token) || {}
  const expires = jwtData.exp || 0
  return new Date().getTime() / 1000 < expires
}
