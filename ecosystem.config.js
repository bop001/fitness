module.exports = {
  apps: [
    {
      name: 'Nuxt Fitnes',
      port: 30003,
      script: './node_modules/nuxt/bin/nuxt.js',
      args: 'start',
      instances: 1,
      autorestart: true
    }
  ]
}
